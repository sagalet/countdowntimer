package someone.greenfox.countdowntimer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ItemDetailActivity extends Activity implements View.OnTouchListener, CompoundButton.OnCheckedChangeListener
        , View.OnFocusChangeListener, View.OnClickListener {

    private final String TAG = "ItemDetailActivity";
    private CountDownItem mItem = null;
    private Button mAccept = null;
    private Button mCancel = null;

    private int mPosition = 0;
    private Ringtones mRingtones = null;

    private Drawable mBackground = null;


    private EditText mEdtTitle = null;
    private EditText mEdtMessage = null;
    private EditText mEdtTimeout = null;
    private TextView mTxtRingtone = null;
    private CheckBox mCbxPreNotify = null;
    private TextView mTxtPreNotifyRingtone = null;
    private EditText mEdtPreNotifiTime = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            mItem = b.getParcelable("item");
            mPosition = b.getInt("position", 0);
            if (mItem != null) {
                initUI(mItem);
            }
        } else {
            initUI(null);
        }

        mCbxPreNotify.setOnCheckedChangeListener(this);

        mAccept = (Button) findViewById(R.id.btn_accept);
        mCancel = (Button) findViewById(R.id.btn_cancel);

        mAccept.setOnClickListener(this);
        mCancel.setOnClickListener(this);

        mRingtones = new Ringtones(this);
        mTxtRingtone.setOnTouchListener(this);

        mEdtPreNotifiTime.setOnFocusChangeListener(this);
    }

    private void setPreNotifyEnabled(boolean enable) {
        TableRow rowr = (TableRow) findViewById(R.id.row_pre_notify_ringtone);
        TableRow rowt = (TableRow) findViewById(R.id.row_pre_notify_time);
        if (enable) {
            mTxtPreNotifyRingtone = (TextView) findViewById(R.id.txt_pre_notify_ringtone);
            mTxtPreNotifyRingtone.setOnTouchListener(this);
            rowr.setBackground(mBackground);
            rowt.setBackground(mBackground);
        } else {
            rowr.setBackgroundColor(Color.LTGRAY);
            rowt.setBackgroundColor(Color.LTGRAY);
            mTxtPreNotifyRingtone.setOnTouchListener(null);
        }
        mEdtPreNotifiTime.setEnabled(enable);

    }

    private void initUI(CountDownItem item) {
        mEdtTitle = (EditText) findViewById(R.id.edx_title);
        mEdtMessage = (EditText) findViewById(R.id.edx_message);
        mEdtTimeout = (EditText) findViewById(R.id.edx_timeout);
        mTxtRingtone = (TextView) findViewById(R.id.txt_ringtone);
        mCbxPreNotify = (CheckBox) findViewById(R.id.cbx_pre_notify);
        mTxtPreNotifyRingtone = (TextView) findViewById(R.id.txt_pre_notify_ringtone);
        mEdtPreNotifiTime = (EditText) findViewById(R.id.edx_pre_notify_time);
        mBackground = mTxtRingtone.getBackground();

        if (item != null) {
            mEdtTitle.setText(item.getTitle());
            mEdtMessage.setText(item.getMessage());
            mEdtTimeout.setText(Integer.toString(item.getTimeout()));
            mTxtRingtone.setText(item.getRingtone());
            mCbxPreNotify.setSelected(item.isPreNotifyEnabled());
            mTxtPreNotifyRingtone.setText(item.getPreRingtone());
            if (item.isPreNotifyEnabled()) {
                mCbxPreNotify.setChecked(true);
                mTxtPreNotifyRingtone.setText(item.getPreRingtone());
                mEdtPreNotifiTime.setText(Integer.toString(item.getPreNotifyTime()));
                setPreNotifyEnabled(true);
                return;
            }
        }
        setPreNotifyEnabled(false);
    }

    private void selectRingtone(final TextView v) {
        RingtoneAlertDialog dialog = new RingtoneAlertDialog(this, v);
        dialog.show();
    }

    private CountDownItem generateItem() {
        StringBuffer buf = new StringBuffer();
        String checkPreNotifyTime = null;
        String title = mEdtTitle.getText().toString();
        if (title == null || title.length() == 0) {
            buf.append(" title,");
        }
        String message = mEdtMessage.getText().toString();
        String timeout = mEdtTimeout.getText().toString();
        int itimeout = 0;
        if (timeout == null || timeout.length() == 0) {
            buf.append(" timeout,");
        } else {
            itimeout = Integer.parseInt(timeout);
        }
        String ringtone = mTxtRingtone.getText().toString();
        if (ringtone == null || ringtone.length() == 0) {
            buf.append(" ringtone,");
        }
        boolean preNotify = mCbxPreNotify.isChecked();
        String preRingtone = mTxtPreNotifyRingtone.getText().toString();
        if (preNotify && (preRingtone == null || preRingtone.length() == 0)) {
            buf.append(" Pre-Notify ringtone,");
        }
        String preTimeout = mEdtPreNotifiTime.getText().toString();
        int ipreTimeout = 0;

        if (preTimeout != null && preTimeout.length() == 0) {
            if (preNotify) {
                buf.append(" Pre-Notify time,");
            }
        } else {
            String check = checkPreNotifyTime();
            if (check != null) {
                checkPreNotifyTime = check;
            } else {
                ipreTimeout = Integer.parseInt(preTimeout);
            }
        }

        if (buf.length() != 0 || checkPreNotifyTime != null) {
            showErrorToast((buf.length() != 0 ? "Please correct fields - " + buf.toString() : "") +
                    (checkPreNotifyTime == null ? "" : "\n" + checkPreNotifyTime));
            return null;
        }

        return new CountDownItem(title, message, itimeout, ringtone, preNotify, ipreTimeout, preRingtone);
    }

    private String checkPreNotifyTime() {
        int limit = 0;
        String time = mEdtPreNotifiTime.getText().toString();
        String timeout = mEdtTimeout.getText().toString();
        if (timeout != null && timeout.length() > 0) {
            limit = Integer.parseInt(timeout) / 2;


            if (time != null && time.length() > 0) {
                if (Integer.parseInt(time) > limit) {
                    Log.i(TAG, "change limit");
                    mEdtPreNotifiTime.setText(Integer.toString(limit));
                    return "the value must less then " + limit + "(timeout/2)";
                }
            }
        } else {
            mEdtPreNotifiTime.setText("");
            return "Set timeout first";
        }
        return null;
    }

    private void showErrorToast(String err) {
        Toast toast = Toast.makeText(this, err, Toast.LENGTH_LONG);
        ViewGroup v = (ViewGroup) toast.getView();
        v.setBackgroundColor(Color.BLACK);
        TextView t = (TextView) v.getChildAt(0);
        t.setTextColor(Color.WHITE);
        toast.show();

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                ColorDrawable background = new ColorDrawable();
                background.setColor(getResources().getColor(R.color.colorAccent));
                v.setBackground(background);
                return true;
            case MotionEvent.ACTION_UP:
                v.setBackground(mBackground);
                selectRingtone((TextView) v);
                return true;

        }
        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setPreNotifyEnabled(isChecked);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Log.i(TAG, "focus=" + hasFocus);
        if (!hasFocus) {
            String check = checkPreNotifyTime();
            if (check != null) {
                showErrorToast(check);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btn_accept:
                CountDownItem item = generateItem();
                if (item != null) {
                    Intent intent = new Intent();
                    Bundle b = new Bundle();
                    b.putParcelable("item", item);
                    b.putInt("position", mPosition);
                    intent.putExtras(b);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
        }
    }

    private class RingtoneAlertDialog implements DialogInterface.OnClickListener {
        private AlertDialog mDialog = null;
        private int mSelect = 0;
        private TextView mView = null;

        RingtoneAlertDialog(Context context, TextView view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Select a ringtone");
            int index = mRingtones.getIndexOf(view.getText().toString());
            mSelect = (index >= 0 ? index : 0);
            builder.setSingleChoiceItems(mRingtones.getRingtoneList(), mSelect, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mRingtones.playRingtone(mRingtones.getRingtoneList()[which]);
                    mSelect = which;
                }
            });

            builder.setPositiveButton("Ok", this);

            builder.setNegativeButton("Cancel", null);
            mDialog = builder.create();
            mView = view;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            mView.setText(mRingtones.getRingtoneList()[mSelect]);
        }

        void show() {
            mDialog.show();
        }
    }
}