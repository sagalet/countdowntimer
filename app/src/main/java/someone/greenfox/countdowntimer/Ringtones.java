package someone.greenfox.countdowntimer;

import android.content.Context;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.ArrayMap;
import android.util.Log;

public class Ringtones {
    private final String TAG = "Ringtones";
    private RingtoneManager mManager = null;
    private Context mContext = null;
    private ArrayMap<String, Integer> mArray = null;

    public Ringtones(Context context) {
        mContext = context;
        mManager = new RingtoneManager(mContext);
        mArray = new ArrayMap<>();
        mManager.setType(RingtoneManager.TYPE_NOTIFICATION);
        Cursor c = mManager.getCursor();
        while (c.moveToNext()) {
            mArray.put(c.getString(RingtoneManager.TITLE_COLUMN_INDEX), c.getPosition());
        }
    }

    public void playRingtone(int position) {
        AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (am.getRingerMode() == AudioManager.RINGER_MODE_NORMAL) {
            Ringtone r = mManager.getRingtone(position);
            r.play();
        } else {
            Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(750);
        }

    }

    public void playRingtone(String title) {
        playRingtone(mArray.get(title));
    }

    public Ringtone getRingtone(int position) {
        return mManager.getRingtone(position);
    }

    public Ringtone getRingtone(String title) {
        if (title == null || title.length() == 0)
            return null;
        return getRingtone(mArray.get(title));
    }

    public String[] getRingtoneList() {
        return mArray.keySet().toArray(new String[mArray.size() - 1]);
    }

    public int getIndexOf(String title) {
        if (title == null || title.length() == 0)
            return -1;
        int index = mArray.indexOfKey(title);
        Log.i(TAG, "title=" + title + " index=" + index);
        return index;
    }

    public int getPosition(String title) {
        if (title == null || title.length() == 0)
            return -1;
        return mArray.get(title);
    }

    public String getTitle(Uri uri) {
        int position = mManager.getRingtonePosition(uri);
        return (position == -1 ? null : mManager.getRingtone(position).getTitle(mContext));
    }

    public String getTitle(int position) {
        Ringtone r = mManager.getRingtone(position);
        return (r == null ? null : r.getTitle(mContext));
    }

    public Uri getUri(String title) {
        if (title == null || title.length() == 0)
            return null;
        int position = mArray.get(title);
        return (position >= 0 ? mManager.getRingtoneUri(position) : null);
    }
}
