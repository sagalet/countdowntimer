package someone.greenfox.countdowntimer;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class ListFragment extends Fragment {

    public static final int NEW_ITEM = 0;
    public static final int MODIFY_ITEM = 1;
    public static final int DELETE_ITEM = 2;
    public static final int CLEAR_ITEM = 4;
    private final String TAG = "ListFragment";
    private DragableListView mListView = null;
    private DragableListViewAdapter mAdapter = null;
    private ArrayList<CountDownItem> mArray = null;
    private OnItemChangeListener mListener = null;
    private Handler mHandler = null;
    private RepeatListAdapter mRepaet = null;

    public ListFragment() {
        mArray = new ArrayList<>();
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (mListener != null) {
                    super.handleMessage(msg);
                    int position = msg.arg1;
                    int status = msg.what;
                    CountDownItem item = (CountDownItem) msg.obj;
                    mListener.onItemChange(status, position, item);
                }
            }
        };
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        mListView = (DragableListView) getView().findViewById(R.id.list_item);

        mAdapter = new DragableListViewAdapter(getContext(), R.layout.dragable_item, mArray) {
            @Override
            public void onClick(int action, int position) {
                switch (action) {
                    case DragableListViewAdapter.MODIFY: {
                        Message msg = mHandler.obtainMessage(MODIFY_ITEM, position, 0, mArray.get(position));
                        mHandler.sendMessage(msg);
                        break;
                    }
                    case DragableListViewAdapter.DELETE: {
                        Log.i(TAG, "data delete");
                        CountDownItem item = mArray.remove(position);
                        this.notifyDataSetChanged();
                        Message msg = mHandler.obtainMessage(DELETE_ITEM, position, 0, item);
                        mHandler.sendMessage(msg);
                        break;
                    }
                }
            }
        };
        mRepaet = new RepeatListAdapter(getContext(), android.R.layout.simple_list_item_1, mArray);
        mListView.setAdapter(mAdapter);
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_list, container, false);
    }

    public int size() {
        return mArray.size();
    }

    public boolean addItem(CountDownItem item) {
        if (mArray == null) {
            return false;
        }
        Log.i(TAG, "addItem = " + item);
        mArray.add(item);
        mAdapter.notifyDataSetChanged();
        Message msg = mHandler.obtainMessage(NEW_ITEM, mArray.indexOf(item), 0, item);
        mHandler.sendMessage(msg);
        return true;
    }

    public boolean addItem(CountDownItem item, int position) {
        if (mArray == null || position > mArray.size()) {
            return false;
        }
        mArray.add(position, item);
        mAdapter.notifyDataSetChanged();
        Message msg = mHandler.obtainMessage(NEW_ITEM, position, 0, item);
        mHandler.sendMessage(msg);
        return true;
    }

    public boolean removeItem(int position) {
        if (mArray == null || position >= mArray.size()) {
            return false;
        }
        CountDownItem item = mArray.remove(position);
        mAdapter.notifyDataSetChanged();
        Message msg = mHandler.obtainMessage(DELETE_ITEM, position, 0, item);
        mHandler.sendMessage(msg);
        return true;
    }

    public void clear() {
        mArray.clear();
        mAdapter.notifyDataSetChanged();
        Message msg = mHandler.obtainMessage(CLEAR_ITEM);
        mHandler.sendMessage(msg);
    }

    public void registerOnItemChangeListener(OnItemChangeListener listener) {
        mListener = listener;
    }

    public CountDownItem[] getAllItems() {
        if (mArray == null || mArray.size() == 0) {
            return null;
        }
        return mArray.toArray(new CountDownItem[mArray.size()]);
    }

    public String[] getAllItemTitles() {
        ArrayList<String> titles = new ArrayList<>();
        CountDownItem[] items = getAllItems();
        if (items == null || items.length == 0) {
            return null;
        }
        for (CountDownItem item : items) {
            titles.add(item.getTitle());
        }
        return titles.toArray(new String[titles.size()]);
    }

    public void showRepeatDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getResources().getString(R.string.select_items));
        View v = getLayoutInflater().inflate(R.layout.repeat_list, null);
        ListView list = (ListView) v.findViewById(R.id.list_repeat);
        list.setAdapter(mRepaet);
        builder.setView(v);
        builder.show();
    }

    public interface OnItemChangeListener {
        public abstract void onItemChange(int status, int position, CountDownItem item);
    }


}
