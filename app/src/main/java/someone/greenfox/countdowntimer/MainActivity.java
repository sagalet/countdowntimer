package someone.greenfox.countdowntimer;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements ListFragment.OnItemChangeListener {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private final String TAG = "CountDown Timer";
    private ListFragment mList = null;
    private Menu mMenu = null;
    private boolean mIsRunning = false;
    private File mCurrentFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.i(TAG, "onCreate");
        mList = new ListFragment();
        mList.registerOnItemChangeListener(this);
        setFragment(mList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu");
        ActionBar action = getSupportActionBar();
        action.setDisplayShowTitleEnabled(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.i(TAG, "onPrepareOptionsMenu");
        mMenu = menu;
        checkButton();
        return true;
    }

    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (resultCode == RESULT_OK) {
            Bundle b = data.getExtras();
            CountDownItem item = (CountDownItem) b.getParcelable("item");
            switch (requestCode) {
                case ListFragment.NEW_ITEM:
                    Log.i(TAG, "item = " + item);
                    mList.addItem(item);
                    break;
                case ListFragment.MODIFY_ITEM:
                    int position = b.getInt("position");
                    mList.removeItem(position);
                    mList.addItem(item, position);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.add_item:
                Intent intent = new Intent(MainActivity.this, ItemDetailActivity.class);
                startActivityForResult(intent, ListFragment.NEW_ITEM);
                break;
            case R.id.clear_item:
                mList.clear();
                checkButton();
                break;
            case R.id.play_item: {
                CountDownItem[] items = mList.getAllItems();
                if (item != null) {
                    mIsRunning = true;
                    checkButton();
                    FragmentManager fm = getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    CountDownFragment fragment = new CountDownFragment();
                    Bundle b = new Bundle();
                    b.putParcelableArray("items", items);
                    fragment.setArguments(b);
                    ft.replace(R.id.frm_main, fragment);
                    ft.commit();
                }
                break;
            }
            case R.id.load_item: {
                if (checkIfChanged(mCurrentFile)) {
                    showChangeCheckedDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showLoadFileDialog();
                        }
                    }, null);
                } else {
                    showLoadFileDialog();
                }
                break;
            }
            case R.id.save_item: {
                if (checkIfChanged(mCurrentFile)) {
                    if (mCurrentFile == null) {
                        showSaveFileDialog();
                    } else {
                        mCurrentFile.delete();
                        mCurrentFile = writeToFile(mCurrentFile.getName());
                        checkButton();
                    }
                }
                break;
            }
            case R.id.new_item: {
                if (checkIfChanged(mCurrentFile)) {
                    showChangeCheckedDialog(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            reset();
                        }
                    }, null);
                } else {
                    reset();
                }
                Log.i(TAG, "check button is gone!?!?");
                break;
            }
            case R.id.delete_file: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getResources().getString(R.string.check_delete) + " \"" +
                        mCurrentFile.getName() + "\"?");
                builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCurrentFile.delete();
                        reset();
                    }
                });
                builder.setNegativeButton(R.string.cancel, null);
                builder.show();
                break;
            }
            case R.id.add_repeat: {
                mList.showRepeatDialog();
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_VOLUME_UP:
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    AudioManager manager = (AudioManager) getSystemService(AUDIO_SERVICE);
                    int adjust = keyCode == KeyEvent.KEYCODE_VOLUME_UP ? AudioManager.ADJUST_RAISE :
                            AudioManager.ADJUST_LOWER;
                    manager.adjustSuggestedStreamVolume(adjust, AudioManager.STREAM_NOTIFICATION, 0);
                    return true;
                case KeyEvent.KEYCODE_BACK:
                    if (mIsRunning) {
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frm_main, mList);
                        ft.commit();
                        mIsRunning = false;
                        checkButton();
                        return true;
                    }
                    break;
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    private void setFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frm_main, fragment);
        ft.commit();
    }

    private void checkButton() {
        if (mIsRunning) {
            mMenu.clear();
        } else {
            if (mMenu.size() == 0) {
                getMenuInflater().inflate(R.menu.menu_main, mMenu);
            }
            mMenu.setGroupVisible(R.id.group_total_item, true);
            mMenu.setGroupVisible(R.id.group_has_item,
                    mList.size() != 0);
            if (mCurrentFile != null) {
                mMenu.setGroupVisible(R.id.group_file_loaded, true);
            } else {
                mMenu.setGroupVisible(R.id.group_file_loaded, false);
            }
        }
    }

    private void showChangeCheckedDialog(DialogInterface.OnClickListener positive,
                                         DialogInterface.OnClickListener negative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.check_discard);
        builder.setPositiveButton(R.string.accept, positive);
        builder.setNegativeButton(R.string.cancel, negative);
        builder.show();
    }

    private File writeToFile(String name) {
        File dir = getExternalFilesDir("item");
        File f = null;
        try {

            f = new File(dir, name);
            f.createNewFile();

            FileOutputStream out = new FileOutputStream(f);
            CountDownItem[] items = mList.getAllItems();
            JSONArray array = getJsonArrayFromItems(items);
            if (array != null || array.length() != 0) {
                String str = array.toString();
                if (str != null) {
                    out.write(str.getBytes());
                    out.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    private void showSaveFileDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText edit = new EditText(this);
        edit.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setTitle(R.string.enter_file_name);
        builder.setView(edit);
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCurrentFile = writeToFile(edit.getText().toString());
                ;
                checkButton();
            }
        });
        builder.show();
    }

    private void showLoadFileDialog() {
        File dir = getExternalFilesDir("item");
        final File[] files = dir.listFiles();
        if (files.length <= 0) {
            return;
        }
        final ArrayList<String> list = new ArrayList<>();
        for (File f : files) {
            list.add(f.getName());
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.select_a_file).setItems(list.toArray(new String[list.size()]),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File f = files[which];
                        reset();
                        try {
                            CountDownItem[] items = getItemsFromString(getStringFromFile(f));
                            if (items != null) {
                                for (CountDownItem i : items
                                        ) {
                                    mList.addItem(i);
                                }
                            }
                            mCurrentFile = f;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });
        builder.setCancelable(true);
        builder.show();
    }

    private boolean checkIfChanged(File file) {
        // there are some items now
        CountDownItem[] items = mList.getAllItems();
        if (items != null && items.length != 0) {
            String str1 = null;
            try {
                str1 = getStringFromFile(file);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            String str2 = getJsonArrayFromItems(items).toString();
            if (str1 == null) {
                if (str2 != null) {
                    return true;
                }
            } else {
                if (!str1.equals(str2)) {
                    return true;
                }
            }
        }
        return false;
    }

    private String getStringFromFile(File file) throws IOException {
        if (file == null) {
            return null;
        }
        StringBuffer buffer = new StringBuffer();
        FileInputStream in = new FileInputStream(file);
        byte[] buf = new byte[1024];

        while (in.read(buf) != -1) {
            buffer.append(new String(buf));
        }

        return buffer.toString().trim();
    }

    private CountDownItem[] getItemsFromString(String str) throws JSONException {
        if (str == null) {
            return null;
        }

        JSONArray array = new JSONArray(str);
        int length = array.length();
        if (length == 0) {
            return null;
        }

        CountDownItem[] items = new CountDownItem[length];
        for (int i = 0; i < length; i++) {
            items[i] = new CountDownItem(array.getJSONObject(i));
        }
        return items;
    }

    private JSONArray getJsonArrayFromItems(CountDownItem[] items) {
        if (items == null) {
            return null;
        }
        JSONArray array = new JSONArray();
        for (CountDownItem i : items
                ) {
            array.put(i.toJsonObj());
        }
        return array;
    }

    private void reset() {
        mCurrentFile = null;
        mList.clear();
        checkButton();
    }

    @Override
    public void onItemChange(int status, int position, CountDownItem item) {
        switch (status) {
            case ListFragment.MODIFY_ITEM:
                Log.i(TAG, "data modify");
                Intent intent = new Intent(MainActivity.this, ItemDetailActivity.class);
                Bundle b = new Bundle();
                b.putParcelable("item", item);
                b.putInt("position", position);
                intent.putExtras(b);
                startActivityForResult(intent, ListFragment.MODIFY_ITEM);
                break;
            case ListFragment.NEW_ITEM:
            case ListFragment.DELETE_ITEM:
                checkButton();
                break;

        }
    }
}
