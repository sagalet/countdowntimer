package someone.greenfox.countdowntimer;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class CountDownFragment extends Fragment implements View.OnClickListener {

    private final int PLAY = 0;
    private final int SET = 1;
    private final int RESET = 2;
    private final int STOP = 3;
    private String TAG = "CountDownFragment";
    private TextView mTxtTimeout = null;
    private Handler mHandler = null;
    private Ringtones mRingtones = null;
    private LocalCountDownTimer[] mList = null;
    private LocalCountDownTimer mCurrent = null;

    private boolean mRunning = false;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        TextView v = view.findViewById(R.id.txt_title);
        resizeTextView(v, 2);
        v = view.findViewById(R.id.txt_message);
        resizeTextView(v, 3);
        mTxtTimeout = view.findViewById(R.id.txt_timeout);
        resizeTextView(mTxtTimeout, 0.5f);
        mRingtones = new Ringtones(getContext());

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case PLAY:
                        Log.i(TAG, "play ringtone " + (String) msg.obj);
                        mRingtones.playRingtone((String) msg.obj);
                        break;
                    case SET:
                        CountDownItem item = (CountDownItem) msg.obj;
                        View view = getView();
                        TextView v = (TextView) view.findViewById(R.id.txt_title);
                        v.setText(item.getTitle());
                        v = (TextView) view.findViewById(R.id.txt_message);
                        v.setText(item.getMessage());
                        break;
                    case RESET:
                        boolean running = mCurrent.isRunning();
                        resetCurrent();
                        if (running) {
                            mCurrent.start();
                        }
                        break;
                    case STOP:
                        stopCountDown();
                        reset();
                        break;
                }
            }
        };
        CountDownItem[] items = (CountDownItem[]) getArguments().getParcelableArray("items");
        if (items != null) {
            mList = new LocalCountDownTimer[items.length];
            LocalCountDownTimer next = null;
            for (int i = items.length - 1; i >= 0; i--) {
                mList[i] = new LocalCountDownTimer(mHandler, items[i], next);
                next = mList[i];
            }
            mCurrent = mList[0];
        }

        ImageView img = (ImageView) getView().findViewById(R.id.img_play_pause);
        img.setOnClickListener(this);
        img = (ImageView) getView().findViewById(R.id.img_reset);
        img.setOnClickListener(this);
        img = (ImageView) getView().findViewById(R.id.img_stop);
        img.setOnClickListener(this);
        startCountDown();
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mCurrent != null && !mCurrent.isRunning() && mRunning) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mCurrent.start();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mCurrent != null && mCurrent.isRunning() && mRunning) {
            mCurrent.pause();
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.count_down, container, false);
        Log.i(TAG, "onCreateView");
        return view;
    }

    @Override
    public void onClick(View v) {
        if (mCurrent == null) {
            return;
        }
        ImageView img = (ImageView) v;
        switch (img.getId()) {
            case R.id.img_play_pause:
                if (mCurrent.isRunning()) {
                    stopCountDown();
                    img.setImageResource(R.drawable.play);
                } else {
                    img.setImageResource(R.drawable.pause);
                    startCountDown();
                }
                break;
            case R.id.img_reset:
                mHandler.sendEmptyMessage(RESET);
                break;
            case R.id.img_stop:
                mHandler.sendEmptyMessage(STOP);
                break;
        }
    }

    private void resizeTextView(TextView view, float mul) {
        WindowManager d = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        d.getDefaultDisplay().getMetrics(dm);
        float size = (dm.widthPixels / dm.density) / view.getMaxEms() * mul;
        view.setTextSize(size);
    }

    private void reset() {
        ImageView i = getView().findViewById(R.id.img_play_pause);
        i.setImageResource(R.drawable.play);
        resetCurrent();
    }

    private void resetCurrent() {
        if (mCurrent != null && mCurrent.isRunning()) {
            mCurrent.cancel();
        }
        mCurrent = mList[0];
        mCurrent.reset();
        mCurrent.initUI();
    }

    private void startCountDown() {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mRunning = true;
        mCurrent.start();
    }

    private void stopCountDown() {
        if (mCurrent != null && mCurrent.isRunning()) {
            mCurrent.pause();
        }
        mRunning = false;
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private class LocalCountDownTimer {

        private CountDownItem mItem = null;
        private LocalCountDownTimer mNext = null;
        private CountDownTimer mTimer = null;
        private long mTimeout = 0l;
        private boolean mRunning = false;
        private Handler mHandler = null;

        public LocalCountDownTimer(@NonNull Handler handler, @NonNull final CountDownItem item, @Nullable final LocalCountDownTimer next) {
            mItem = item;
            mNext = next;
            mHandler = handler;
            mTimeout = item.getTimeout();
        }

        public CountDownTimer start() {
            initUI();
            mRunning = true;
            mTimer = new CountDownTimer(mTimeout * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    synchronized (this) {
                        if (mRunning) {
                            mTimeout = millisUntilFinished / 1000;
                            mTxtTimeout.setText(Long.toString(mTimeout));
                            if (mTimeout == 0) {
                                mHandler.sendMessageDelayed(mHandler.obtainMessage(PLAY, mItem.getRingtone()),
                                        millisUntilFinished > 100 ? millisUntilFinished - 100 : millisUntilFinished);
                            }
                            if (mItem.isPreNotifyEnabled()) {
                                if (millisUntilFinished > mItem.getPreNotifyTime() * 1000 &&
                                        millisUntilFinished < (mItem.getPreNotifyTime() + 1) * 1000) {
                                    mHandler.sendMessageDelayed(mHandler.obtainMessage(PLAY, mItem.getPreRingtone()),
                                            millisUntilFinished - mItem.getPreNotifyTime() * 1000 > 100 ?
                                                    millisUntilFinished - mItem.getPreNotifyTime() * 1000 - 100 :
                                                    millisUntilFinished - mItem.getPreNotifyTime() * 1000);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFinish() {
                    Log.i(TAG, "onFinish");
                    mCurrent = mNext;
                    if (mNext != null) {
                        reset();
                        mNext.start();
                    } else {
                        mHandler.sendEmptyMessage(STOP);
                    }
                }
            };
            return mTimer.start();
        }

        public void cancel() {
            synchronized (this) {
                mRunning = false;
                mTimer.cancel();
            }
        }

        public void pause() {
            synchronized (this) {
                mRunning = false;
                mTimer.cancel();
            }
        }

        public boolean isRunning() {
            return mRunning;
        }

        public void initUI() {
            mTxtTimeout.setText(Long.toString(mTimeout));
            mHandler.sendMessage(mHandler.obtainMessage(SET, mItem));
        }

        public void reset() {
            mTimeout = mItem.getTimeout();
        }
    }

}
