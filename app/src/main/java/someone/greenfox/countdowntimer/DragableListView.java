package someone.greenfox.countdowntimer;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class DragableListView extends ListView {

    private final String TAG = "DragableListView";

    public DragableListView(Context context) {
        this(context, null);
    }

    public DragableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
