package someone.greenfox.countdowntimer;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public class CountDownItem implements Parcelable {

    public static final Creator<CountDownItem> CREATOR = new Creator<CountDownItem>() {
        @Override
        public CountDownItem createFromParcel(Parcel in) {
            return new CountDownItem(in);
        }

        @Override
        public CountDownItem[] newArray(int size) {
            return new CountDownItem[size];
        }
    };
    private final String TAG = "CountDownItem";
    private final String JSON_TITLE = "title";
    private final String JSON_MESSAGE = "message";
    private final String JSON_TIMEOUT = "timeout";
    private final String JSON_RINGTONE = "ringtone";
    private final String JSON_PRE_NOTIFY = "pre-notify";
    private final String JSON_PRE_NOTIFY_TIME = "pre-notify-time";
    private final String JSON_PRE_NOTIFY_RINGTONE = "pre-notify-ringtone";
    private final String JSON_REPEAT_TIMES = "repeat-times";
    private final String JSON_REPEAT_NEXT = "repeat-next";

    private String mTitle = null;
    private String mMessage = null;
    private int mTimeout = 0;
    private String mRingtone = null;
    private boolean mSetPreNotify = false;
    private int mPreNotifyTime = 0;
    private String mPreRingtone = null;
    private int mRepeat = 0;
    private int mRepeatNext = -1;

    public CountDownItem() {
        this(null, null);
    }

    public CountDownItem(String title, String message) {
        this(title, message, 0, null);
    }

    public CountDownItem(String title, String message, int timeout, String ringtone) {
        this(title, message, timeout, ringtone, false, 0, null);
    }

    public CountDownItem(String title, String message, int timeout, String ringtone,
                         boolean setPreNotify, int preNotifyTime, String preRingtone) {
        mTitle = title;
        mMessage = message;
        mTimeout = timeout;
        mRingtone = ringtone;
        mPreNotifyTime = preNotifyTime;
        mSetPreNotify = setPreNotify;
        mPreRingtone = preRingtone;
    }

    public CountDownItem(@NonNull JSONObject object) {
        try {
            mTitle = object.getString(JSON_TITLE);
            mMessage = object.getString(JSON_MESSAGE);
            mTimeout = object.getInt(JSON_TIMEOUT);
            mRingtone = object.getString(JSON_RINGTONE);
            mSetPreNotify = object.getBoolean(JSON_PRE_NOTIFY);
            mPreNotifyTime = object.getInt(JSON_PRE_NOTIFY_TIME);
            mPreRingtone = object.getString(JSON_PRE_NOTIFY_RINGTONE);
            mRepeat = object.getInt(JSON_REPEAT_TIMES);
            mRepeatNext = object.getInt(JSON_REPEAT_NEXT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected CountDownItem(Parcel in) {
        mTitle = in.readString();
        mMessage = in.readString();
        mTimeout = in.readInt();
        mRingtone = in.readString();
        mSetPreNotify = in.readByte() != 0;
        mPreNotifyTime = in.readInt();
        mPreRingtone = in.readString();
        mRepeat = in.readInt();
        mRepeatNext = in.readInt();
    }

    public String setPreNotify(boolean enable, int timeout, String ringtone) {
        mSetPreNotify = enable;
        String msg = null;
        if (timeout >= mTimeout / 2) {
            mPreNotifyTime = mTimeout / 2;
            msg = "Pre-Notify time should not greater than " + mTimeout / 2;
        } else {
            mPreNotifyTime = timeout;
        }
        mPreRingtone = ringtone;
        return msg;
    }

    public boolean isPreNotifyEnabled() {
        return mSetPreNotify;
    }

    public String getRingtone() {
        return mRingtone;
    }

    public void setRingtone(String ringtone) {
        mRingtone = ringtone;
    }

    public String getPreRingtone() {
        return mPreRingtone;
    }

    public int getPreNotifyTime() {
        return mPreNotifyTime;
    }

    public int getTimeout() {
        return mTimeout;
    }

    public void setTimeout(int timeout) {
        mTimeout = timeout;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public int getRepeat() { return mRepeat; }

    public void setRepeat(int repeat) { mRepeat = repeat; }

    public void setNextRepeat(int next) { mRepeatNext = next; }

    public int getNextRepeat() { return mRepeatNext; }

    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("Item {");
        buf.append(" title = " + mTitle);
        buf.append(" message = " + mMessage);
        buf.append(" timeout = " + mTimeout);
        buf.append(" Ringtone = " + (mRingtone == null ? null : mRingtone.toString()));
        buf.append(" setPreNotify = " + mSetPreNotify);
        if (mSetPreNotify) {
            buf.append(" preNotifyTime = " + mPreNotifyTime);
            buf.append(" preRingtone = " + (mPreRingtone == null ? null : mPreRingtone.toString()));
        }
        buf.append(" repest = " + mRepeat);
        buf.append(" repest next = " + mRepeatNext);
        buf.append("}");
        return buf.toString();
    }

    public JSONObject toJsonObj() {
        JSONObject object = new JSONObject();
        try {
            object.put(JSON_TITLE, mTitle);
            object.put(JSON_MESSAGE, mMessage);
            object.put(JSON_TIMEOUT, mTimeout);
            object.put(JSON_RINGTONE, mRingtone);
            object.put(JSON_PRE_NOTIFY, mSetPreNotify);
            object.put(JSON_PRE_NOTIFY_TIME, mPreNotifyTime);
            object.put(JSON_PRE_NOTIFY_RINGTONE, mPreRingtone);
            object.put(JSON_REPEAT_TIMES, mRepeat);
            object.put(JSON_REPEAT_NEXT, mRepeatNext);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return object;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mMessage);
        dest.writeInt(mTimeout);
        dest.writeString(mRingtone);
        dest.writeByte((byte) (mSetPreNotify ? 1 : 0));
        dest.writeInt(mPreNotifyTime);
        dest.writeString(mPreRingtone);
        dest.writeInt(mRepeat);
        dest.writeInt(mRepeatNext);
    }

}
