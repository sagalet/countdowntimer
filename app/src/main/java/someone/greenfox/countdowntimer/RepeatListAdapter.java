package someone.greenfox.countdowntimer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class RepeatListAdapter extends ArrayAdapter<CountDownItem> {

    private Context mContext = null;

    public RepeatListAdapter(@NonNull Context context, int resource, ArrayList<CountDownItem> array) {
        super(context, resource, array);
        mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;
        ViewHolder vh = null;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1, null);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewHolder vh = (ViewHolder) v.getTag();
                    vh.onClick();
                }
            });

            vh = new ViewHolder(view);
            view.setTag(vh);
        } else {
            view = convertView;
            vh = (ViewHolder) view.getTag();
        }
        vh.refresh(getItem(position));
        return view;
    }

    private class ViewHolder {

        private View mView = null;
        private CountDownItem mItem = null;

        ViewHolder(View v) {
            mView = v;
        }

        void refresh(CountDownItem item) {
            mItem = item;
            TextView txt = (TextView) mView.findViewById(android.R.id.text1);
            txt.setText(item.getTitle());

        }

        void onClick() {
            if (mItem.getRepeat() != 0) {
                mView.setBackgroundColor(0xffffffff);
                mItem.setRepeat(0);
            } else {
                mView.setBackgroundColor(0xddcc8822);
                mItem.setRepeat(3);
            }
        }
    }
}
