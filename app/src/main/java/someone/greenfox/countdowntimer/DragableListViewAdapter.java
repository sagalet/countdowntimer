package someone.greenfox.countdowntimer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;

public abstract class DragableListViewAdapter extends ArrayAdapter<CountDownItem> {

    public static final int MODIFY = 0;
    public static final int DELETE = 1;

    private static final String TAG = "DragableListViewAdapter";
    private Context mContext = null;
    private boolean mLongPress = false;
    private View mDraggedView = null;

    public DragableListViewAdapter(@NonNull Context context, int resource, @NonNull ArrayList<CountDownItem> array) {
        super(context, resource, array);
        mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        long checkstart = System.nanoTime();
        View view = null;
        ViewHolder vh = null;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.dragable_item, null);
            vh = new ViewHolder(view);
            view.setTag(vh);
            TableLayout itemBrief = (TableLayout) view.findViewById(R.id.tbl_item_brief);
            itemBrief.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewHolder vh = (ViewHolder) v.getTag();
                    int position = vh.getPostion();
                    DragableListViewAdapter.this.onClick(MODIFY, position);
                }
            });

            itemBrief.setTag(vh);
            itemBrief.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    mLongPress = true;
                    ViewHolder vh = (ViewHolder) v.getTag();
                    Log.i(TAG, "onLongClick position=" + vh.getPostion());
                    final float x = vh.getX();
                    final float y = vh.getY();
                    mDraggedView = v;
                    v.startDrag(null, new View.DragShadowBuilder(v) {
                        @Override
                        public void onProvideShadowMetrics(Point outShadowSize,
                                                           Point outShadowTouchPoint) {
                            super.onProvideShadowMetrics(outShadowSize, outShadowTouchPoint);
                            outShadowTouchPoint.x = (int) x;
                            outShadowTouchPoint.y = (int) y;
                        }

                        @Override
                        public void onDrawShadow(Canvas canvas) {
                            super.onDrawShadow(canvas);
                        }

                    }, v, 0);
                    //v.setVisibility(View.INVISIBLE);
                    return true;
                }
            });

            itemBrief.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            ViewHolder vh = (ViewHolder) v.getTag();
                            vh.setPoint(event.getX(), event.getY());
                            break;
                        case MotionEvent.ACTION_UP:
                            if (mLongPress) {
                                mLongPress = false;
                            } else {
                                return v.performClick();
                            }
                    }
                    return false;
                }
            });

            itemBrief.setOnDragListener(new View.OnDragListener() {
                @Override
                public boolean onDrag(View v, DragEvent event) {
                    //Log.i(TAG, "onDrag event=" + event.getAction() + "position="+ (int)v.getTag(R.id.tag_position));
                    switch (event.getAction()) {
                        case DragEvent.ACTION_DRAG_STARTED:
                            break;
                        case DragEvent.ACTION_DRAG_ENDED:
                            mDraggedView = null;
                            v.setVisibility(View.VISIBLE);
                            break;
                        case DragEvent.ACTION_DRAG_ENTERED:
                            if (mDraggedView != null) {
                                ViewHolder from = (ViewHolder) mDraggedView.getTag();
                                ViewHolder to = (ViewHolder) v.getTag();
                                swap(from.getPostion(), to.getPostion());
                                mDraggedView = v;
                            }
                            break;
                    }
                    return true;
                }
            });

            ImageView imtDel = (ImageView) view.findViewById(R.id.img_delete_item);
            imtDel.setTag(vh);
            imtDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewHolder vh = (ViewHolder) v.getTag();
                    DragableListViewAdapter.this.onClick(DELETE, vh.getPostion());
                }
            });
        } else {
            view = convertView;
            vh = (ViewHolder) view.getTag();
        }
        CountDownItem item = (CountDownItem) getItem(position);
        vh.setPosition(position);
        vh.refresh(item);
        //Log.i(TAG, "performance = " + (System.nanoTime() - checkstart));
        return view;
    }

    private void swap(int from, int to) {
        if ((from >= getCount() || to >= getCount()) ||
                (from < 0 || to < 0) ||
                from == to) {
            return;
        }
        CountDownItem item = getItem(from);
        remove(item);
        insert(item, to);
    }

    public abstract void onClick(int action, int position);

    public static class ViewHolder {
        private RelativeLayout mView = null;
        private CountDownItem mItem;
        private float mX;
        private float mY;
        private int mPosition;

        public ViewHolder(View v) {
            mView = (RelativeLayout) v;
        }

        public void refresh(CountDownItem item) {
            mItem = item;
            TextView title = (TextView) mView.findViewById(R.id.txv_title);
            title.setText(item.getTitle());
            TextView ringtone = (TextView) mView.findViewById(R.id.txv_ringtone);
            ringtone.setText(item.getRingtone());
            TextView timeout = (TextView) mView.findViewById(R.id.txv_timeout);
            timeout.setText(Integer.toString(item.getTimeout()));
        }

        public void setPosition(int position) {
            mPosition = position;
        }

        public int getPostion() {
            return mPosition;
        }

        public void setPoint(float x, float y) {
            mX = x;
            mY = y;
            Log.i(TAG, "x=" + mX + " y=" + mY);
        }

        public float getX() {
            return mX;
        }

        public float getY() {
            return mY;
        }
    }
}
